# coding: utf-8
import argparse
import os
import sys
import json
import numpy as np
import pandas as pd
from sklearn.metrics import auc, precision_recall_curve, roc_auc_score, roc_curve
from xgboost import XGBClassifier
from statistics import mean



def printf(format, *args):
    """C style printf"""

    sys.stdout.write(format % args)

def fprintf(fp, format, *args):
    """C style fprintf"""

    fp.write(format % args)


def train_model(features, labels, wpos, nfeat):
    """Trains an xgboost classifier with the given features"""

    model = XGBClassifier(nthread=12, scale_pos_weight=wpos, eval_metric="logloss")

    model.fit(features, labels)  # fit model to training data

    return model


def predict(feature_row, model, iout=False):
    """apply xgb model"""

    proba = model.predict_proba(feature_row)
    pos_proba = proba[:, 1]

    if iout:
        predictions = [round(value) for value in pos_proba]
        return proba, predictions

    return proba, pos_proba


def loocv_split(feats_np, labels_np, b):
    """split the data using one donor as the test set"""

    mask = np.ones(len(labels_np), dtype=bool)
    mask[b,] = False
    xtest, xtrain = feats_np[b], feats_np[mask]
    ytest, ytrain = labels_np[b], labels_np[mask]

    return xtrain, xtest, ytrain, ytest


def make_roc_plot0(fplot, ytest, proba):

    import matplotlib.pyplot as plt
    import scikitplot as skplt

    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(10, 10))

    skplt.metrics.plot_roc_curve(ytest, proba)

    fig.tight_layout()
    plt.savefig(fplot)


def make_roc_plot(fplot, ytest, pos_proba, legend):
    import matplotlib.pyplot as plt
    auc = roc_auc_score(ytest, pos_proba)
    # summarize scores
    textbox= legend + ' ROC AUC=%.3f' % (auc)
    # calculate roc curves
    
    fpr, tpr, _ = roc_curve(ytest, pos_proba)
    
    # plot the roc curve for the model

    plt.plot([0, 1], [0, 1], "r--")
    plt.axis(xmin=0, xmax=1)
    plt.fill_between(fpr,0,tpr,alpha=0.2)
    plt.plot(fpr, tpr, marker='.', label=textbox)
    # axis labels
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    # show the legend
    plt.legend()
    # save the plot
    plt.savefig(fplot)



def make_overlay_roc_plot(fplot, ytest1, proba1,legend1,ytest2, proba2, legend2):
    import matplotlib.pyplot as plt
    auc1 = roc_auc_score(ytest1, proba1)
    textbox1 = legend1 + ' AUC=%.3f' % (auc1)
    fpr1, tpr1, _ = roc_curve(ytest1, proba1)

    auc2 = roc_auc_score(ytest2, proba2)
    textbox2 = legend2 + ' AUC=%.3f' % (auc2)

    fpr2, tpr2, _ = roc_curve(ytest2, proba2)


    plt.rc('font', size=14)          # controls default text sizes
    plt.rc('xtick', labelsize=14)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=14)    # fontsize of the tick labels    
    plt.axis(xmin=0, xmax=1)

    fig1, ax = plt.subplots()
    ax.set_aspect('equal', adjustable='box')

    #plt.fill_between(fpr,0,tpr,alpha=0.2)
    plt.plot(fpr1, tpr1, marker='o', label=textbox1)
    plt.plot(fpr2, tpr2, marker='o', label=textbox2)
    plt.plot([0, 1], [0, 1], "k--")

    # axis labels
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    # show the legend
    plt.legend()
    # save the plot

    plt.legend(loc="lower right")

    plt.savefig(fplot)




def make_overlay_pr_plot(fplot, ytest1, pos_proba1,legend1,ytest2, pos_proba2, legend2):
    import matplotlib.pyplot as plt

    expect1= np.sum(ytest1)/len(ytest1)

    p1,r1, hold_threshold1 = precision_recall_curve(ytest1, pos_proba1)
    auc1 = auc(r1, p1)

    #loocv_precision, loocv_recall, _ = precision_recall_curve(all_y, all_ypred)
    #pr_auc = auc(loocv_recall, loocv_precision)

    textbox1 = legend1 + ' AUC=%.3f (Expect %.3f)' % (auc1,expect1)
    #fpr1, tpr1, _ = roc_curve(ytest1, proba1)
    p2, r2, hold_threshold2 = precision_recall_curve(ytest2, pos_proba2)
    auc2 = auc(r2, p2)

    expect2= np.sum(ytest2)/len(ytest2)

    #auc2 = roc_auc_score(ytest2, proba2)
    textbox2 = legend2 + ' AUC=%.3f (Expect %.3f)' % (auc2,expect2)

    plt.rc('font', size=14)          # controls default text sizes
    plt.rc('xtick', labelsize=14)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=14)    # fontsize of the tick labels    
    plt.axis(xmin=0, xmax=1)

    fig1, ax = plt.subplots()
    ax.set_aspect('equal', adjustable='box')


    plt.plot(r1, p1, marker='o', label=textbox1)
    plt.plot(r2, p2, marker='o', label=textbox2)

    # axis labels
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    # show the legend
    plt.legend()
    # save the plot

    plt.legend(loc="lower left")

    plt.savefig(fplot)


# # Parsing all arguments
# parser = argparse.ArgumentParser(description="prepare ROC and PR plots and perform LOOCV")
# parser.add_argument("-i", dest="fin", required=True, help="input file")
# parser.add_argument("-o", dest="out_file", help="output file for LOOCV etc")
# parser.add_argument("-g", dest="group_column", default="Donor", help="group by this column")
# parser.add_argument("-max_col", default=1000000, type=int, help="maxium number of columns (features)")
# #parser.add_argument("-max_col", default=100, type=int, help="maxium number of columns (features)")
# parser.add_argument("-p", dest="plotfile", help="ROC Plot")
# parser.add_argument("--loocv", action="store_true", help="Perform LOOCV")
# parser.add_argument("-v", dest="verbose", action="store_true", help="verbose output")
# parser.add_argument("--ihold", help="holdout features")
# parser.add_argument("--seed", type=int, default=42, help="random seed")
# parser.add_argument("-b", dest="balance", action="store_true", help="balance training input")
# args = parser.parse_args()

max_col = 1000000
loocv = True

feat_dir = r'./2024-bib-data/example/results/feat_para_80_80'

for each_train_fin in [x for x in os.listdir(feat_dir) if 'train_feat' in x]:

    fin = os.path.join(feat_dir,each_train_fin)
    ihold = fin.replace('train_feat','hold_feat')

    dout = os.path.abspath(os.path.join(fin, os.pardir))

    dmin = os.path.basename(fin).replace(".tsv", "").split("_")[-1]

    plotfile = os.path.join(feat_dir,'_hold_roc_%s.png'%dmin)

    out_file = os.path.join(feat_dir,'hold_auc_%s.tsv'%dmin)

    print ('>>>Dmin',dmin)

    df_train = pd.read_table(fin)

    labels_loo = df_train["label"].values
    groups_loo = df_train["Donor"].values
    feat_cols = []

    if ihold:
        df_hold = pd.read_table(ihold)
        labels_hold = df_hold["label"].values
        groups_hold = df_hold["Donor"].values


        for j in range(0, max_col):
            c = "c" + str(j)
            if c in df_train.columns and c in df_hold.columns:
                feat_cols.append(c)
    else:
        for j in range(0, max_col):
            c = "c" + str(j)
            if c in df_train.columns:
                feat_cols.append(c)


    nfeat = len(feat_cols)
    feats_loo = df_train[feat_cols].values

    if ihold:
        feats_hold = df_hold[feat_cols].values


    fdata_out = os.path.join(dout,"loo-" + str(dmin) + ".json")
    data_out = {}
    data_out["hold"] = []
    data_out["looo"] = []
    nhold=0
    hold_roc_auc = 0.0
    hold_pr_auc = 0.0
    if  ihold:

        # for i in range(0, len(labels_hold)):
        #     print(i, labels_hold[i], groups_hold[i], feats_hold[i][0:5])

        if len(set(labels_hold)) <= 1:
            print("only one class in holdout")
            sys.exit()


        roc_auc = 0.0
        pr_auc = 0.0
        xtrain = feats_loo
        ytrain = labels_loo
        xtest = feats_hold
        ytest = labels_hold
        nhold = len(ytest)
        ntrue = np.sum(ytrain)
        nfalse = len(ytrain) - ntrue
        wpos = nfalse / ntrue
        hold_model = train_model(xtrain, ytrain, wpos, nfeat)
        hold_proba,hold_ypred = predict(xtest, hold_model)
        hold_roc_auc = roc_auc_score(ytest, hold_ypred)
        hold_precision, hold_recall, hold_threshold = precision_recall_curve(ytest, hold_ypred)
        hold_pr_auc = auc(hold_recall, hold_precision)
        fplot=""


        train_important_feature_table = os.path.join(dout,"train_important_feat_" + str(dmin) + ".tsv")
        importance = hold_model.feature_importances_
        train_feature_importances = pd.DataFrame(importance, index=feat_cols, columns=['importance']).sort_values('importance',
                                                                                                                  ascending=False)
        train_feature_importances.index.rename('Feature', inplace=True)

        sorted_train_feature_importances = train_feature_importances[train_feature_importances['importance'] > 0.0]
        sorted_train_feature_importances.to_csv(train_important_feature_table, sep='\t', index=True)


        for i in range(0,len(ytest)):
            item = [groups_hold[i], str(hold_ypred[i]), str(ytest[i])]
            data_out["hold"].append(item)


        hold_ytest=ytest


    if loocv:
        all_ypred = []
        all_y = []

        for irow in range(len(labels_loo)):
            xtrain, xtest, ytrain, ytest = loocv_split(feats_loo, labels_loo, [irow])

            ntrue = np.sum(ytrain)
            nfalse = len(ytrain) - ntrue
            wpos = nfalse / ntrue

            model = train_model(xtrain, ytrain, wpos, nfeat)

            y_proba, y_pred = predict(xtest, model)

            all_ypred.append(y_pred[0])
            all_y.append(ytest[0])

            item = [groups_loo[irow], str(y_pred[0]), str(ytest[0])]
            data_out["looo"].append(item)

        roc_auc = roc_auc_score(all_y, all_ypred)
        loocv_precision, loocv_recall, _ = precision_recall_curve(all_y, all_ypred)
        pr_auc = auc(loocv_recall, loocv_precision)

        print(roc_auc,dout,plotfile)


        if plotfile:
            fplot = os.path.join(dout, plotfile)

            filename, file_extension = os.path.splitext(plotfile)
            prfile=filename + "_PR" + file_extension
            fprlot = os.path.join(dout, prfile)

            if ihold:
                make_overlay_roc_plot(fplot, hold_ytest, hold_ypred,"Holdout", all_y, all_ypred, "LOOCV")
                make_overlay_pr_plot(fprlot, hold_ytest, hold_ypred,"Holdout", all_y, all_ypred, "LOOCV")

            else:
                make_roc_plot(fplot, all_y, all_ypred, "LOOCV")
        if out_file:
            fh = open(out_file, 'w')
        else:
            fh = sys.stdout


        fprintf(fh,
            "%3d\t%8.3f\t%8.3f\t%3d\t%8.3f\t%8.3f\t%s\t%s\n",
            nhold,
            hold_roc_auc,
            hold_pr_auc,
            len(ytrain),
            roc_auc,
            pr_auc,
            dmin,fplot
        )

        with open(fdata_out, 'w') as fp:
            json.dump(data_out, fp)
