# Companion Data for BiB paper
***Title: Robust diagnosis of infectious disease, autoimmunity and cancer from the paratope networks of adaptive immune receptors***

Zichang Xu, Hendra S Ismanto, Dianita S Saputri, Soichiro Haruna, Guanqun Sun, Jan Wilamowski, Shunsuke Teraguchi, Ayan Sengupta, Songling Li, Daron M Standley

Preprint link: [https://www.biorxiv.org/content/10.1101/2023.11.28.569125v5](URL)


## Example
We have chosen the COVID-19 (BCR) case as an example.

### Input data
The **2024-bib-data/example/data** folder contains the heavy chain information from COVID-19 patients and healthy donors. Each unique individual has both **XXX-pseudo_joined.fa** and **XXX.tsv** files.
* **XXX-pseudo_joined.fa**: pseudo sequence consisting of CDR1, 2, and 3 (using IMGT annotation scheme).
* **XXX.tsv**: meta information of each sequence including amino acids of variable region, V gene, and J gene, etc.

### Training/Testing Results
In the **2024-bib-data/example/results** folder, **donor_list.tsv** records the "donor" and "class" (1: Disease patient, 0: Healthy donor) information. We provide feature folders based on 4 proposed methods:
* **feat_clono_80_nosearch**: Clonotype Cluster Frequency (CCF) method.
* **feat_clono_80_80**: Clonotype Cluster Occupancy (CCO) method. 
* **feat_para_80_nosearch**: Paratope cluster frequency (PCF) method.
* **feat_para_80_80**: Paratope Cluster Occupancy (PCO) method.

In each feature folder, the **train_feat_Dmin.tsv** was used to train the XGBoost model and test on leave-one-out data **hold_feat_Dmin.tsv**. The pipeline will train different *Dmin* features from 0.0, 0.1, 0.2 ~ 0.899. The training and testing ROC AUC and PR AUC are reported in hold_auc_Dmin.tsv. We optimized the hyperparameter *Dmin* value based on examining the highest training ROC AUC of each XGBoost model across a series of *Dmin* values and report its corresponding testing ROC AUC in this study.

## The code for training XGBoost model
We have uploaded the local version of the training XGBoost model and perform leave-one-out testing. The code can be found in **train_xgb_local.py**. Users can change the feature directory `feat_dir = r'./2024-bib-data/example/results/feat_para_80_80'` to other benchmark features.


## Input data for Benchmark
In **2024-bib-data/data**, the AIR data of six diseases from three categories (infectious disease, autoimmune disease, and cancer) are stored. Please refer to Table S1 for detailed information on each disease.

* **COVID.tgz**: BCRs of 44 Coronavirus disease 2019 (COVID-19) patients and 58 healthy donors.
* **HIV.tgz**: BCRs of 94 Human immunodeficiency virus (HIV) patients and 128 healthy donors.
* **AIH.tgz**: TCRs of 59 Autoimmune hepatitis (AIH) patients and 59 healthy
donors.
* **T1D.tgz**: TCRs of 34 Type 1 Diabetes (T1D) patients and 11 healthy donors.
* **NSCLC.tgz**: TCRs of 204 Non-small cell lung cancer (NSCLC) patients and 294 healthy donors from seven studies.
* **CRC.tgz**: TCRs of 20 Colorectal cancer (CRC) patients and 88 healthy donors.

## Results
### Benchmark
The training and testing results of XGBoost models based on CCF, CCO, PCF, and PCO features among COVID, HIV, AIH, T1D, NSCLC, and CRC disease cohorts are stored in  **2024-bib-data/results/benchmark**.

### NSCLC batch effect
The NSCLC cohort consisted of 3 NSCLC studies and 4 Healthy studies. We constructed the holdout test set from one healthy study and one NSCLC study, with the others used for training. Twelve splits of training and test sets were formed. We performed training and testing of 12 experiments separately (**2024-bib-data/results/NSCLC_batch_effect**). The summary table can be found in **NSCLC_batch_effect.xlsx**. 



PS: For some cases, please perform `cat XXX.tgz_part_* > XXX.tgz` before unzipping the file.